package com.example.demo.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.cache.annotation.CacheResult;
import javax.inject.Named;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Value;

import com.example.demo.AppProperties.CrawlerProperties;
import com.exmple.demo.interfaces.ICrawlingService;
import com.exmple.demo.model.PageInfo;
import com.exmple.demo.model.PageTreeInfo;

import lombok.extern.slf4j.Slf4j;


@Slf4j
@Named
public class ICrawlingServiceImpl implements ICrawlingService {

	    @Value("#{appProperties.crawler}")
	    private CrawlerProperties crawlerProperties;

	    /*
	     * recursive crawler to fetch child pages upto desired depth / max depth
	     * (non-Javadoc)
	     *
	     */
	    @Override
	    @CacheResult(cacheName = "web-crawler")
	    public PageTreeInfo deepCrawl(final String url, final int depth, final List<String> processedUrls) {

	        log.debug("Starting crawler for url {} for depth {}", url, depth);
	        if (depth < 0) {
	            log.info("Maximum depth reached, backing out for url {}", url);
	            return null;
	        } else {
	            final List<String> updatedProcessedUrls = Optional.ofNullable(processedUrls).orElse(new ArrayList<>());
	            if (!updatedProcessedUrls.contains(url)) {
	                updatedProcessedUrls.add(url);
	                final PageTreeInfo pageTreeInfo = new PageTreeInfo(url);
	                crawl(url).ifPresent(pageInfo -> {
	                    pageTreeInfo.title(pageInfo.getTitle()).valid(true);
	                    log.info("Found {} links on the web page: {}", pageInfo.getLinks().size(), url);
	                    pageInfo.getLinks().forEach(link -> {
	                        pageTreeInfo.addNodesItem(deepCrawl(link.attr("abs:href"), depth - 1, updatedProcessedUrls));
	                        /** .below commented can be used to returns a list of media and imports here **/
	                        //pageTreeInfo.addNodesItem(deepCrawl(link.attr("abs:src"), depth - 1, updatedProcessedUrls));
	                        //pageTreeInfo.addNodesItem(deepCrawl(link.attr("rel"), depth - 1, updatedProcessedUrls));
	                    });
	                });
	                return pageTreeInfo;
	            } else {
	                return null;
	            }
	        }

	    }

	    /*
	     * Method to fetch web page content. Cache is used for better performance
	     *
	     */
	    @Override
	    @CacheResult(cacheName = "web-crawler")
	    public Optional<PageInfo> crawl(final String url) {

	        log.info("Fetching contents for url: {}", url);
	        try {
	            final Document doc = Jsoup.connect(url).timeout(crawlerProperties.getTimeOut())
	                    .followRedirects(crawlerProperties.isFollowRedirects()).get();

	            /** .select returns a list of links here **/
	            final Elements links = doc.select("a[href~=(?i)(wiprodigital.com)]");
	            
	            
	            
	            /** .below commented can be used to returns a list of media and imports here **/
	            /*final Elements media = doc.select("[src]");
	            final Elements imports = doc.select("link[href]");
	            
	            print("\nMedia: (%d)", media.size());
	            
	            for (Element src : media) {
	                if (src.tagName().equals("img"))
	                    print(" * %s: <%s> %sx%s (%s)",
	                            src.tagName(), src.attr("abs:src"), src.attr("width"), src.attr("height"),
	                            trim(src.attr("alt"), 20));
	                else
	                    print(" * %s: <%s>", src.tagName(), src.attr("abs:src"));
	            }
	            
	            print("\nImports: (%d)", imports.size());
	            
	            for (Element link : imports) {
	                print(" * %s <%s> (%s)", link.tagName(),link.attr("abs:href"), link.attr("rel"));
	            }*/
	            
	            
	            final String title = doc.title();
	            log.debug("Fetched title: {}, links[{}] for url: {}", title, links.nextAll(), url);
	            return Optional.of(new PageInfo(title, url, links));
	        } catch (final IOException | IllegalArgumentException e) {
	            log.error(String.format("Error getting contents of url %s", url), e);
	            return Optional.empty();
	        }

	    }
	    
	    private static String trim(String s, int width) {
	        if (s.length() > width)
	            return s.substring(0, width-1) + ".";
	        else
	            return s;
	    }
	    
	    private static void print(String msg, Object... args) {
	        System.out.println(String.format(msg, args));
	    }
}
