package com.example.demo.api;

import java.util.Optional;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.demo.AppProperties.CrawlerProperties;
import com.exmple.demo.interfaces.ICrawlingService;
import com.exmple.demo.model.PageTreeInfo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
@Api(value = "Web Crawler", description = "Operations pertaining to crawl a website with depth")
public class CrawlerApiController {

    @Inject
    private ICrawlingService crawlerService;

    @Value("#{appProperties.crawler}")
    private CrawlerProperties crawlerProperties;
    
    @GetMapping(value = "/crawl", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<PageTreeInfo> getWebPageTreeInfo(
            @NotNull @RequestParam(value = "url", required = true) final String url,
            @RequestParam(value = "depth", required = false) final Integer depth) {

        log.info("Request for deep crawling received for url: {}, depth: {}", url, depth);
        final int newDepth = Integer.min(Optional.ofNullable(depth).orElse(crawlerProperties.getDefaultDepth()),
                crawlerProperties.getMaxDepthAllowed());
        log.info(
                "Depth might be optimized to go upto Max defined in property:'app.crawler.max-depth-allowed'. optimized depth: {}",
                newDepth);
        return new ResponseEntity<>(crawlerService.deepCrawl(url, depth, null), HttpStatus.OK);
    }

}
