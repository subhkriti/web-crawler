package com.example.demo.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import javax.inject.Inject;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.Cache;
import org.springframework.cache.interceptor.SimpleKey;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.CrawlerIntegrationTest;
import com.exmple.demo.interfaces.ICrawlingService;
import com.exmple.demo.model.PageInfo;
import com.exmple.demo.model.PageTreeInfo;


@RunWith(SpringRunner.class)
@CrawlerIntegrationTest
public class CrawlerServiceCacheTest {

    @Value("#{cacheManager.getCache('${spring.application.name}')}")
    private Cache applicationCache;

    @Inject
    private ICrawlingService crawlerService;

    @Test
    public void testCacheOnDeepCrawl() {
        final PageTreeInfo info = crawlerService.deepCrawl("http://wiprodigital.com", 0, null);
        assertThat(info).isNotNull().satisfies(treeInfo -> {
            assertThat(treeInfo.getTitle()).contains("Digital Transformation - Wipro Digital");
            assertThat(treeInfo.getUrl()).contains("http://wiprodigital.com");
        });
        assertThat(applicationCache.getName()).isNotNull();
        assertThat(applicationCache.get(new SimpleKey("http://wiprodigital.com", 0, null))).isNotNull();
    }

    @Test
    public void testCacheOnCrawl() {
        final Optional<PageInfo> info = crawlerService.crawl("http://wiprodigital.com");
        assertThat(info).isPresent();
        assertThat(info.get().getTitle()).contains("Digital Transformation - Wipro Digital");
        assertThat(info.get().getUrl()).contains("http://wiprodigital.com");
        assertThat(info.get().getLinks().size()).isGreaterThan(10);

        assertThat(applicationCache.get("http://wiprodigital.com")).isNotNull();
    }

}
