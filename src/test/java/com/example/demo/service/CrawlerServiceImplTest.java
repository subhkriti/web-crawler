package com.example.demo.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import javax.inject.Inject;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.CrawlerIntegrationTest;
import com.exmple.demo.interfaces.ICrawlingService;
import com.exmple.demo.model.PageInfo;
import com.exmple.demo.model.PageTreeInfo;



@RunWith(SpringRunner.class)
@CrawlerIntegrationTest
public class CrawlerServiceImplTest {

    @Inject
    private ICrawlingService crawlerService;

    @Test
    public void testDeepCrawl() {
        final PageTreeInfo info = crawlerService.deepCrawl("http://wiprodigital.com", 1, null);
        assertThat(info).isNotNull().satisfies(treeInfo -> {
            assertThat(treeInfo.getTitle()).contains("Digital Transformation - Wipro Digital");
            assertThat(treeInfo.getUrl()).contains("http://wiprodigital.com");
            assertThat(treeInfo.getNodes().size()).isGreaterThan(10);
        });
    }

    @Test
    public void testCrawl() {
        final Optional<PageInfo> info = crawlerService.crawl("http://wiprodigital.com");
        assertThat(info).isPresent();
        assertThat(info.get().getTitle()).contains("Digital Transformation - Wipro Digital");
        assertThat(info.get().getUrl()).contains("http://wiprodigital.com");
        assertThat(info.get().getLinks().size()).isGreaterThan(10);
    }

}
